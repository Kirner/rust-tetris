extern crate piston;
extern crate graphics;
extern crate glutin_window;
extern crate opengl_graphics;

mod tetris_core;
mod graphics_helpers;

use piston::window::WindowSettings;
use piston::event_loop::*;
use piston::input::*;
use glutin_window::GlutinWindow as Window;
use opengl_graphics::{ GlGraphics, OpenGL };

use graphics_helpers::TetrisGrid;

pub struct App {
    tetris_grid: TetrisGrid,
    gl:          GlGraphics,
}

impl App {
    fn render(&mut self, args: &RenderArgs) {
        self.tetris_grid.draw(&mut self.gl, &args);
    }

    fn update(&mut self, _args: &UpdateArgs) {
        // Rotate 2 radians per second.
        //self.rotation += 2.0 * args.dt;
    }
}

fn main() {
    let opengl = OpenGL::V3_2;

    let mut window: Window = WindowSettings::new(
            "Tetris!",
            [418, 838]
        )    
        .opengl(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();

    let tetris_grid = TetrisGrid::new(40, 2);

    let mut app = App {
        tetris_grid: tetris_grid,
        gl:          GlGraphics::new(opengl),
    };

    let mut events = Events::new(EventSettings::new());
    while let Some(e) = events.next(&mut window) {
        if let Some(r) = e.render_args() {
            app.render(&r);
        }

        if let Some(u) = e.update_args() {
            app.update(&u);
        }
    }
}
