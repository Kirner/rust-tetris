extern crate piston;
extern crate graphics;
extern crate opengl_graphics;

use piston::input::*;
use graphics::*;
use opengl_graphics::{ GlGraphics };

const DARK_GREY: [f32; 4] = [0.2, 0.2, 0.2, 1.0];
const BLACK:     [f32; 4] = [0.0, 0.0, 0.0, 1.0];

pub struct TetrisGrid {
    pub cell_size:   f64,
    pub gap_size:    f64,
    pub grid_width:  f64,
    pub grid_height: f64,
    pub win_height:  f64,
    pub win_width:   f64,
}

impl TetrisGrid {
    pub fn new(cell_size: u32, gap_size: u32) -> TetrisGrid {
        let grid_width          = 10;
        let grid_height         = 20;
        let grid_width_ex_gaps  = cell_size * 10;
        let grid_height_ex_gaps = cell_size * 20;
        let win_width           = grid_width_ex_gaps + (gap_size * 9);
        let win_height          = grid_height_ex_gaps + (gap_size * 19);

        TetrisGrid {
            cell_size:   cell_size   as f64,
            gap_size:    gap_size    as f64,
            grid_width:  grid_width  as f64,
            grid_height: grid_height as f64,
            win_width:   win_width   as f64,
            win_height:  win_height  as f64,
        }
    }

    pub fn draw(&mut self, gl: &mut GlGraphics, args: &RenderArgs) {
        let c = &Context::new_abs(self.win_width, self.win_height);

        gl.draw(args.viewport(), |_, gl| {
            clear(BLACK, gl);
            self.draw_blocks(gl, &c);
        });
    }

    fn draw_blocks(&mut self, gl: &mut GlGraphics, c: &Context) {
        let cell_size = self.cell_size as f64;
        let rect2 = rectangle::square(100.0, 0.0, cell_size);
        rectangle(DARK_GREY, rect2, c.transform, gl);
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn creating_a_grid() {
        let grid = TetrisGrid::new(40, 2);
        assert_eq!(40  as f64, grid.cell_size);
        assert_eq!(2   as f64, grid.gap_size);
        assert_eq!(418 as f64, grid.win_width);
        assert_eq!(838 as f64, grid.win_height);
        assert_eq!(10  as f64, grid.grid_width);
        assert_eq!(20  as f64, grid.grid_height);
    }
}
