#[allow(dead_code)]
pub fn question() -> i32 {
    6 * 7
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn the_answer() {
        assert_eq!(42, question());
    }
}
